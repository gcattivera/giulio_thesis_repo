package tcpIpClassifier;

import java.io.*;
import java.util.*;
import weka.core.*;
import weka.core.converters.ConverterUtils.*;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.*;

/**
 * This class contains methods to construct "time intervals" training dataset, 
 * one of these objects is associated to only one user, so the timeIntervals dataset
 * is referred to one user.A file of previously classified TCP/IP
 * connections is required, this class can generates both training and test time intervals, in the latter case
 * classification is not supported, you could use third-party software for this purpose.
 * 
 * @author Giulio Cattivera
 */
public class TimeIntervals {
    
    private String[] ipRangeList= null;
    private Instances timeBands_Dataset = null;                 //dataset
    private Remove tb_filter = null;                            //filter to remove the first attribute during classification, that is the timeband attribute

    /**
     * It constructs this object, in particular it initializes the structure
     * of the training dataset stored in this object, so 
     * it needs an array containing the whole ip ranges used by 
     * the classified tcp/ip connections which will be parsed.
     * 
     * @param datasetName A String that will be used as the relation name of the training dataset.
     * @param ipRangeList An array of Strings which contains all the ip ranges used by the 
     */
    public TimeIntervals (String[] ipRangeList, String datasetName) {
        
        this.ipRangeList = ipRangeList;
        Arrays.sort(this.ipRangeList);    //alphabetic sorting of the iprangeList
        
        /* sets up filter, the first attribute that is the "time band" needs to be excluded during classification */
        tb_filter = new Remove();
        tb_filter.setAttributeIndices("first,first");
        
        /* Becoming creating attributes for the dataset, the FastVector "attributes" is the container */
        FastVector attributes = new FastVector(this.ipRangeList.length + 2);
        
        /* adds the time band's name field */
        attributes.addElement(new Attribute("TimeBand", (FastVector) null));
        
        /* adds ip ranges fields, i.e. an attribute for each iprange */
        for(int i = 0; i<this.ipRangeList.length; i++) {
            attributes.addElement(new Attribute(ipRangeList[i]));
        }
        
        /* adds class value field: 0 not connected, 1 connected */
        FastVector timeBandClassValues = new FastVector(2);
        timeBandClassValues.addElement("0");
        timeBandClassValues.addElement("1");
        attributes.addElement(new Attribute("IS CONNECTED", timeBandClassValues));
        
        /* Creates the dataset, and sets index of the class attribute (the last) */
        timeBands_Dataset = new Instances(datasetName, attributes, 0);
        timeBands_Dataset.setClassIndex(timeBands_Dataset.numAttributes() - 1);
        
    }
    
    
    /**
     * This method is a parser for training connections, it can parse only training connections and
     * can creates the "time intervals" instances whom will be added into the timeBands dataset of this object;
     * WARMING: Using this method with test connections is a semantic mistake.
     * 
     * @param trainConnectionsPath A String that represents the absolute pathname of a text file containing training connections.
     * @param classValues A Map which must contains the true class (as the values) for each time intervals (as the keys).
     */
    public void parserTrainingData (String trainConnectionsPath, Map<String,String> classValues) throws FileNotFoundException {
        createInstances(trainConnectionsPath, classValues, timeBands_Dataset, this.ipRangeList);
    }
    
    
    /**
     * This method creates instances from a file and adds them to the given dataset; It can parse a file containing either
     * classified training connections or test connections, with respect to the following pattern:
     * 
     *      "timeBand,ipRange,connectionClassValue,fmeasure"
     * 
     * where -timeBand is the time interval where the connection occured
     *       -ipRange is the target ip range for the connection
     *       -connectionClassValue is the classValue assigned from a previous classification of the connection
     *       -fmeasure is the one used for the classification of this connection and which refers to the class 
     *        value "MATCH" i.e."is a connection of this user".
     * 
     * @param pathname A String that represents a pathname of a file containing classified training or test connections, but all from the same user.
     * @param classValues A Map of classValue (as values) for each time interval (as keys).
     * @param dataSet a dataset to populate.
     * @param ipRangeList An array of Strings that contains the ip ranges whom have been appeared among these connections
     * @return A List of correctly created instances.
     */
    public List<Instance> createInstances (String pathname, Map<String,String> classValues, Instances dataSet, String[] ipRangeList) throws FileNotFoundException {
        
        File srcFile = new File(pathname);
        Scanner scanFile = new Scanner(srcFile);
        
        String timeBand, ipRange, connClassValue,fmeasure;
        StringTokenizer st;
       
        HashMap<String, HashMap<String,Float>> tBandsMap = new HashMap<String, HashMap<String,Float>>(0);
        HashMap<String, Float> hmSupport;
        float weight;
        
        /* Now Populating the Map, this while selects each connection from the file  */
        while(scanFile.hasNext()) {
            
            /* 1- parsing, that is reading each field of a connection */
            st = new StringTokenizer(scanFile.nextLine(), ",");
            timeBand = st.nextToken();
            ipRange = st.nextToken();
            connClassValue = st.nextToken();
            fmeasure = st.nextToken();
            
            /* 2- populating the map */
            if(connClassValue.equals("USER01") /*|| connClassValue.equals("NOT_USER01")*/) {
                if(tBandsMap.containsKey(timeBand)==false) {
                    hmSupport = new HashMap<String,Float>(0);
                    for(int i=0; i< ipRangeList.length; i++) {
                        hmSupport.put(ipRangeList[i], (float)0);
                    }
                    hmSupport.put(ipRange, Float.parseFloat(fmeasure) );
                    tBandsMap.put(timeBand, hmSupport);
                }
                else {
                    hmSupport = tBandsMap.get(timeBand);
                    weight = hmSupport.get(ipRange);
                    weight =  Float.parseFloat(fmeasure) + weight;
                    hmSupport.put(ipRange, weight);
                    tBandsMap.put(timeBand, hmSupport);
                }
            }
        } // while ends. All connections read and map built.
        
        /* Now the tBandsMap is built. The following method takes it
         * and populates the given dataSet with the information from the map.
         */
        List<Instance> instancesList = updateData(tBandsMap, dataSet, classValues, ipRangeList);
        return instancesList;
    }
    
    /* 
     * This auxiliar method populates a dataset with the information from the given map.
     * 
     * @param a map containing informations for the creation of each instance.
     * @param a dataset that will be built.
     * @param a map of class values. One for each time interval.
     * @param an array of ip ranges which occured among the train connections.
     */
    private List<Instance> updateData (HashMap<String,HashMap<String,Float>> tBandsMap, Instances dataSet, Map<String,String> classValues, String[] ipRangeList ){
       
        Instance instance;
        Attribute supportAtt;
        Object[] tBandsArray =  tBandsMap.keySet().toArray();
        Arrays.sort(tBandsArray);
        List<Instance> instancesList = new ArrayList<Instance>(0);
        
        /* for each time interval */
        for(int k=0; k<tBandsArray.length; k++ ){
            String tBand = (String) tBandsArray[k];
            instance = new Instance(ipRangeList.length+2);
            supportAtt = dataSet.attribute("TimeBand");
            instance.setValue(supportAtt, supportAtt.addStringValue(tBand));
            
            /* for each iprange of this time  */
            for(String ipRange: tBandsMap.get(tBand).keySet()) {
                supportAtt = dataSet.attribute(ipRange);
                instance.setValue(supportAtt, tBandsMap.get(tBand).get(ipRange));
            }
            
            /* an instance is created, now links it with the dataset */
            instance.setDataset(dataSet);
            
            /* if it's not a test instance, then adds it to the dataset */
            if(classValues.isEmpty()==false) {
                instance.setClassValue(classValues.get(tBand));
                dataSet.add(instance);
            }
            instancesList.add(instance);
        }
        return instancesList;
    }
    
    /**
     * This method is static, it calculates the Map containing the "true class" for each time interval, that is: 
     * given the pathname of a directory containing files with classified training connections,
     * then it searches for at least one user connection for each time band.
     * 
     * @param dirPathname A String that represents the absolute pathname of a directory containing files of classified training connections.
     * @return A Map with time intervals as keys, and their's class values as values.
     */
    public static HashMap<String,String> calculatesTrueClass (String dirPathname) throws FileNotFoundException {
        
        File dir = new File(dirPathname);
        File fl[] = dir.listFiles();
        File srcFile;
        Scanner scanFile;
        StringTokenizer st;
        String timeBand;
        HashMap<String, String> classValues  = new HashMap<String, String>(0);
                
        for(int i=0; i<fl.length; i++) {
            
            srcFile= new File(fl[i].toString());
            scanFile= new Scanner(srcFile);
            timeBand = fl[i].toString().substring(fl[i].toString().lastIndexOf("/")+1, fl[i].toString().length()-4);
            timeBand = timeBand.substring(timeBand.indexOf("_")+1, timeBand.length());            
            classValues.put(timeBand, "0");
            
            while(scanFile.hasNextLine() && scanFile.nextLine().charAt(0)== '/') {
                st = new StringTokenizer(scanFile.nextLine(), ",");
                st.nextToken();
                if(st.nextToken().equals("1")) {
                    classValues.put(timeBand, "1");
                    scanFile.close();
                    break; //quits from inner while
                }
            }//end while
        }
        return classValues;
    }

    /**
     * This method generates a .arff file of the "time intervals" dataset of this object
     * 
     * @throws Exception 
     */
    public void generateTrainArff() throws Exception {
        DataSink.write(timeBands_Dataset.relationName() + ".arff", timeBands_Dataset);
    }
    
    /**
     * This method parses classified test connections and generates a .arff dataset of test time intervals that you can
     * classify with some thirdy-pary software.The pattern the content of the given file must respect is: 
     * 
     * "timeBand,ipRange,connectionClassValue,fmeasure"
     * 
     * where -timeBand is the time interval where the connection occured
     *       -ipRange is the target ip range for the connection
     *       -connectionClassValue is the classValue assigned from a previous classification of the connection
     *       -fmeasure is the one used for the classification of this connection and which refers to the class 
     *        value "MATCH" i.e."is a connection of this user".
     * 
     * @param dirPathname A string that represents an absolute pathname of a file containing classified user connections.
     * @param ipRanges An array of String that represents the ip ranges whom appear among connections.
     * @throws Exception 
     */
    public void generateTestArff(String pathname, String[] ipRanges) throws Exception {
        
        Instances testDataset = timeBands_Dataset.stringFreeStructure();
        List<Instance> testTimeIntervals = createInstances(pathname, new HashMap<String,String>(), testDataset, ipRanges);
        testDataset.setRelationName(timeBands_Dataset.relationName()+" TEST");
       
        for(Instance s: testTimeIntervals) {
            testDataset.add(s);
        }
        
        /*
        Remove remove = new Remove();
        remove.setAttributeIndices("first");
        remove.setInputFormat(testDataset);
        Instances filteredData = Filter.useFilter(testDataset, remove);
        filteredData.setRelationName(testDataset.relationName());
        */
        DataSink.write(testDataset.relationName()+".arff", testDataset);  
    }
    /**
     * This method returns the "time intervals" dataset
     * 
     * @return An Instances object that is the "time intervals" dataset.
     */
    public Instances getDataSet() {
        return timeBands_Dataset;
    }
  
}
