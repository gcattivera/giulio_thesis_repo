package tcpIpClassifier;

import weka.classifiers.*;
import weka.core.*;
import weka.filters.*;
import java.util.*;
import java.io.*;
import java.lang.reflect.Constructor;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.RandomForest;
import weka.core.converters.ConverterUtils.*;
import weka.filters.supervised.instance.Resample;
import weka.filters.unsupervised.attribute.Remove;
import weka.classifiers.functions.LibSVM;

/**
 * This class represents a classifier for mobile user TCP/IP connections all collected using NetFlow protocol, 
 * A data mining Java API is used, that is WEKA v3.6 Java API;
 * for more information about WEKA library visit http://www.cs.waikato.ac.nz/ml/weka/
 * for more information about NetFlow protcol visit http://en.wikipedia.org/wiki/NetFlow
 *
 * @author Giulio Cattivera
 */
public class TcpIpClassifier {

    /* This array contains a list of classifiers  */
    private List<Classifier> u_Classifiers = new ArrayList<Classifier>();
    /* The current classifier that is used to classify */
    private Classifier currentClassifier = null;
    /* the fmeasure of the currentClassifier that is written in the predictions.txt */
    private double currentFmeasure;
    /* The dataset will contain the training data */
    private Instances usersNetFlows_Dataset = null;
    /* A filter which removes attributes */
    private Remove remove_filter = null;
    /* A filter to resample the dataset in case instances aren't enough */
    private Resample resample_filter = null;
    /* Whether the model is up to date */
    private boolean m_UpToDate=false;
    /* a list of Evaluation object */
    private List<Evaluation> evaluationList = new ArrayList<Evaluation>(0);

    /**
     * Constructs this object by initializing the structure of the dataset and by setting 
     * SVM, RandomForest and NaiveBayes (in this order) as default classifiers.
     * 
     * @throws Exception
     * @see Exception
     */
    public TcpIpClassifier() throws Exception {
        
        /* Name of Dataset 
         * (this initialization is necessary to use correctly the constructor, but later this name will be replaced
         * by the parser() method of this class, with the iprange which identifies and labels a single object of type TcpIpClassifier)
         */
        String nameOfDataSet = "not defined";

        /* initializes filters */
        remove_filter = new Remove();
        remove_filter.setAttributeIndices("first,2");
        resample_filter = new Resample();
        resample_filter.setBiasToUniformClass(1.0);
        resample_filter.setSampleSizePercent(100);

        /* Becoming creating attributes, the FastVector "attributes" is the container */
        FastVector attributes = new FastVector(16);

        attributes.addElement(new Attribute("IpRange", (FastVector) null));

        FastVector vecTrueClass = new FastVector(2);
        vecTrueClass.addElement("0");
        vecTrueClass.addElement("1");
        attributes.addElement(new Attribute("TrueClass", vecTrueClass));

        FastVector vecDirection = new FastVector(3);
        vecDirection.addElement("0");
        vecDirection.addElement("1");
        vecDirection.addElement("2");
        attributes.addElement(new Attribute("Direction", vecDirection));

        attributes.addElement(new Attribute("IN_GAP"));
        attributes.addElement(new Attribute("OUT_GAP"));
        attributes.addElement(new Attribute("AVERAGE BYTE IN_NETFLOWS"));
        attributes.addElement(new Attribute("TOTAL BYTE IN_NETFLOWS"));
        attributes.addElement(new Attribute("AVERAGE BYTE OUT_NETFLOWS"));
        attributes.addElement(new Attribute("TOTAL BYTE OUT_NETFLOWS"));
        attributes.addElement(new Attribute("AVERAGE PKT IN_NETFLOWS"));
        attributes.addElement(new Attribute("TOTAL PKT IN_NETFLOWS"));
        attributes.addElement(new Attribute("AVERAGE PKT OUT_NETFLOWS"));
        attributes.addElement(new Attribute("TOTAL PKT OUT_NETFLOWS"));
        attributes.addElement(new Attribute("AVERAGE DURATION"));
        attributes.addElement(new Attribute("TOTAL DURATION"));

        /* this is the class attribute which tells if the connection comes from the user or not. */
        FastVector userClassValues = new FastVector(2);
        userClassValues.addElement("USER01");
        userClassValues.addElement("NOT_USER01");

        attributes.addElement(new Attribute("USER PROFILE", userClassValues));

        /* Create a dataset, and set index of the class attribute (the last) */
        usersNetFlows_Dataset = new Instances(nameOfDataSet, attributes, 0);
        usersNetFlows_Dataset.setClassIndex(usersNetFlows_Dataset.numAttributes() - 1);
        
        /* builds default Classifiers */
        LibSVM svm = new LibSVM();
        RandomForest rf = new RandomForest();
        NaiveBayes nb = new NaiveBayes();
        u_Classifiers.add(svm);
        u_Classifiers.add(rf);
        u_Classifiers.add(nb);

    }

    /**
     * Constructs the object by initializing the Dataset with the information into the given ".arff" file;
     * there are no controls about the semantic of the content, so it is assumed that the given .arff reflects a
     * dataset previously built by an object of this class.
     * 
     * @param pathname The pathname of a .arff file
     * @throws Exception
     * @see Exception
     */
    public TcpIpClassifier(String pathname) throws Exception {
        
        /* initialize filter: it will remove the first and the second attribute from the model */
        remove_filter = new Remove();
        remove_filter.setAttributeIndices("first,2");
        resample_filter = new Resample();
        resample_filter.setBiasToUniformClass(1.0);
        resample_filter.setSampleSizePercent(100);
        
        /* reads and creates instances */
        usersNetFlows_Dataset = DataSource.read(pathname);
        usersNetFlows_Dataset.setClass(usersNetFlows_Dataset.attribute("USER PROFILE"));
        
        /* builds classifiers */
        LibSVM svm = new LibSVM();
        RandomForest rf = new RandomForest();
        NaiveBayes nb = new NaiveBayes();
        u_Classifiers.add(svm);
        u_Classifiers.add(rf);
        u_Classifiers.add(nb);
    }

    /**
     * Auxiliary method,
     * it updates the given dataset using the given fields of a connection;
     * if you want to use this method to create an instance that you want to classify then
     * the class value for this connection (which is indicated by the param userProfile),
     * must be set to the value "do not set".
     * 
     * @param ipRange A string that represents the target ip range of this connection.
     * @param trueClass An int that in case of training connection represents the "true" class value for this connection.
     * @param direction An int that represents the direction of the connection, 0 for outgoing,1 for ingoing, 2 mixed.
     * @param in_gap A float that represents the average gap among ingoing NetFlows of this connection.
     * @param out_gap A float that represents the average gap among outgoing NetFlows of this connection.
     * @param avg_byte_inNetflows A float that represents the average bytes of the ingoing NetFlows of this connection.
     * @param tot_byte_inNetflows A float that represents the total bytes of the ingoing NetFlows of this connection.
     * @param avg_byte_outNetflows A float that represents the average bytes of the outgoing NetFlows of this connection.
     * @param tot_byte_outNetflows A float that represents the total bytes of the outgoing NetFlows of this connection.
     * @param avg_pkt_inNetflows A float that represents the average packets of the ingoing NetFlows of this connection.
     * @param tot_pkt_inNetflows A float that represents the total packets of the ingoing NetFlows of this connection.
     * @param avg_pkt_outNetflows A float that represents the average packets of the outgoing NetFlows of this connection.
     * @param tot_pkt_outNetflows A float that represents the total packets of the outgoing NetFlows of this connection.
     * @param avg_duration A float that represents the average duration of all NetFlows of this connection.
     * @param tot_duration A float that represents the total duration of all NetFlows of this connection.
     * @param connectionsType A string that represents the classValue for this connection.
     * @param dataSet An Instances object that represents the dataSet where in case of training connection, the instance will be added.
     * @return An Instance object that represent this connection.
     * @see Instances
     * @see Instance
     */
    private Instance updateData(String ipRange, int trueClass, String direction, float in_gap, float out_gap,
            float avg_byte_inNetflows, float tot_byte_inNetflows, float avg_byte_outNetflows, float tot_byte_outNetflows,
            float avg_pkt_inNetflows, float tot_pkt_inNetflows, float avg_pkt_outNetflows, float tot_pkt_outNetflows,
            float avg_duration, float tot_duration, String connectionsType, Instances dataSet) {

        /* Create an empty instance of length 16 */
        Instance instance = new Instance(16);
        Attribute supportAtt;

        // Set the given values in the instance attributes
        supportAtt = dataSet.attribute("IpRange"); //1
        instance.setValue(supportAtt, supportAtt.addStringValue(ipRange));

        supportAtt = dataSet.attribute("TrueClass"); //2
        instance.setValue(supportAtt, (double) trueClass); //don't warry in case of connection to classify the filter won't consider this attribute

        supportAtt = dataSet.attribute("Direction"); //3
        instance.setValue(supportAtt, supportAtt.indexOfValue(direction));

        supportAtt = dataSet.attribute("IN_GAP"); //4
        instance.setValue(supportAtt, (double) in_gap);

        supportAtt = dataSet.attribute("OUT_GAP"); //5
        instance.setValue(supportAtt, (double) out_gap);

        supportAtt = dataSet.attribute("AVERAGE BYTE IN_NETFLOWS"); //6
        instance.setValue(supportAtt, (double) avg_byte_inNetflows);

        supportAtt = dataSet.attribute("TOTAL BYTE IN_NETFLOWS"); //7
        instance.setValue(supportAtt, (double) tot_byte_inNetflows);

        supportAtt = dataSet.attribute("AVERAGE BYTE OUT_NETFLOWS"); //8
        instance.setValue(supportAtt, (double) avg_byte_outNetflows);

        supportAtt = dataSet.attribute("TOTAL BYTE OUT_NETFLOWS"); //9
        instance.setValue(supportAtt, (double) tot_byte_outNetflows);

        supportAtt = dataSet.attribute("AVERAGE PKT IN_NETFLOWS"); //10
        instance.setValue(supportAtt, (double) avg_pkt_inNetflows);

        supportAtt = dataSet.attribute("TOTAL PKT IN_NETFLOWS"); //11
        instance.setValue(supportAtt, (double) tot_pkt_inNetflows);

        supportAtt = dataSet.attribute("AVERAGE PKT OUT_NETFLOWS");  //12
        instance.setValue(supportAtt, (double) avg_pkt_outNetflows);

        supportAtt = dataSet.attribute("TOTAL PKT OUT_NETFLOWS"); //13
        instance.setValue(supportAtt, (double) tot_pkt_outNetflows);

        supportAtt = dataSet.attribute("AVERAGE DURATION"); //14
        instance.setValue(supportAtt, (double) avg_duration);

        supportAtt = dataSet.attribute("TOTAL DURATION"); //15
        instance.setValue(supportAtt, (double) tot_duration);
        
        /* Give instance access to attribute information from the dataset. */
        instance.setDataset(dataSet);
        
        if (connectionsType.equals("do not set") == false) {
            // Sets class value for this instance.
            instance.setClassValue(connectionsType); //16
            // Add instance to training data.
            dataSet.add(instance);
            /* The Dataset is changed */
            m_UpToDate = false;
        }
        
        return instance;
    }

    /**
     * This method is the parser for training connections; it parses all training connections within a text file 
     * and builds the dataset of this object; the dataset's name will be the target ip-range of these connections,
     * the dataset's name is considerd a unique label for identifying a TcpIpClassifier object; the method could be used
     * also to add further training connections to an existing TcpIpClassifier object;
     * WARNING: you must use this method only to parse training connections.
     * 
     * @param path A string that represents an absolute pathname of a text file containing training connections. 
     * @param connectionsType A string that indicates 
     * @return The number of correctly parsed connections.
     * @throws FileNotFoundException 
     */
    public int parserTrainingConnections(String path) throws FileNotFoundException, Exception {

        ArrayList<Instance> al_Instance = new ArrayList<>(0);
        
        /* creates instances from this connections file */
        al_Instance.addAll(createInstances(path, "set", usersNetFlows_Dataset));
        /*
        if(al_Instance.size()>0) {
            resample_filter.setInputFormat(usersNetFlows_Dataset);
            Instances filteredData = Filter.useFilter(usersNetFlows_Dataset, resample_filter);
            filteredData.setRelationName(usersNetFlows_Dataset.relationName());
            usersNetFlows_Dataset = filteredData;
        }
        */
        return al_Instance.size();
        
    }

    /**
     * Creates dataset instances reading connections from the given text file;
     * it can create either instances intended for training, or instances intended for classifications;
     * in the first case (training connections) the method puts the instances into the given dataSet 
     * only if the input parameter "connectionsType" is equal to "set", in the latter case (connection to classify)
     * the method will not modify the given dataSet only if the parameter "connectionType" is equal to "do not set";
     * 
     * So, to sum up: put the "set" value for parameter connectionType when you are going to create training instances, on the other hand put "do not set" value 
     * if you are going to create instances to classify.
     *
     * @param connectionsFilePath String that represents the pathname of a file containing connections
     * @param connectionsType A String that indicates if connections within the file are for training or for classifications purposes,
     *        (you must use "set" in case of training connections, or "do not set" in case of classifications purposes)
     * @param dataSet dataSet to be updated in case of training connections. 
     * @return an ArrayList of Instance, which contains the set of connections succesfully added
     * into the given dataSet, WARNING: it's size could be 0 if in case of training connections, there is a lack of user connections or not_user connections
     */
    public ArrayList<Instance> createInstances(String connectionsFilePath, String connectionsType, Instances dataSet) throws FileNotFoundException {

        String netFlow;                                       //a single netflow entry
        File srcFile = new File(connectionsFilePath);         //sequencial access file to read a subset of user's connections
        Scanner scanFile = new Scanner(srcFile);
        StringTokenizer stSkip;                               //used to skip the not required fileds of a test connection, during it's parsing
        StringTokenizer stNetFlows;                           //to distinguish netflows of a connection
        StringTokenizer stSingleNetflow;                      //to distinguish fileds of a netflow  
        StringTokenizer stIpRange;                            //to parse the ip range of this connection
        String line;                                          //pointer to the text lines (connections)
        ArrayList<Instance> instanceList = new ArrayList<Instance>(0); //the list of connections returned from the function
        Instance instance;

        /* fileds of the attribute scheme */
        String ipRange, userProfile, direction = "";

        float support = 0, in_gap = 0, out_gap = 0, avg_byte_inNetflows = 0, tot_byte_inNetflows = 0,
                avg_byte_outNetflows = 0, tot_byte_outNetflows = 0, avg_pkt_inNetflows = 0, tot_pkt_inNetflows = 0,
                avg_pkt_outNetflows = 0, tot_pkt_outNetflows = 0, avg_duration = 0, tot_duration = 0;

        int port = 0, trueClass = 0, inNetflowNumber = 0, outNetFlowNumber = 0, count;

        boolean thereAreSomeUserConnection = false, thereAreSomeNotUserConnection = false;

        /* If this file contains only train connections
         * i.e. connections are not for classification: */
        if (connectionsType.equals("do not set") == false) {
            count = connectionsFilePath.lastIndexOf(":");
            port = Integer.parseInt(connectionsFilePath.substring(count + 1));
        }

        /* Parsing the ip range of this connection which lies within the given connectionFilePath,
         * then the latter and port number are assigned to the relation name of UsersNetFlows_Dataset,
         * thus they are considered a unique label for an object of type TcpIpClassifier. 
         * This action of labeling, as shown above, must to be taken only the first time, that is when the first instance
         * of UserNetFlows_Dataset is created.
         * 
         * note: otherwise if a test connection is going to be parsed,
         * then the relationName can not be equal to "not defined"
         */
        if (dataSet.relationName().equals("not defined")) { //that is, if this is the first instance
            count = connectionsFilePath.lastIndexOf(":");
            stIpRange = new StringTokenizer(connectionsFilePath.substring(0, count), "/");
            count = stIpRange.countTokens();
            while (count != 1) {
                stIpRange.nextToken();
                count--;
            }
            ipRange = (stIpRange.nextToken()).substring(6);
            dataSet.setRelationName(ipRange + ":" + port); //name and unique label created
        }

        /* Parsing all the other fields, this while scans all connections */
        while (scanFile.hasNextLine() && scanFile.nextLine().charAt(0) == '/') {

            in_gap = 0;
            out_gap = 0;
            inNetflowNumber = 0;
            outNetFlowNumber = 0;
            avg_byte_inNetflows = 0;
            tot_byte_inNetflows = 0;
            avg_byte_outNetflows = 0;
            tot_byte_outNetflows = 0;
            avg_pkt_inNetflows = 0;
            tot_pkt_inNetflows = 0;
            avg_pkt_outNetflows = 0;
            tot_pkt_outNetflows = 0;
            avg_duration = 0;
            tot_duration = 0;
            support = 0;                    //will help with calculations
            avg_byte_inNetflows = 0;
            avg_pkt_inNetflows = 0;
            avg_byte_outNetflows = 0;
            avg_pkt_outNetflows = 0;
            avg_duration = 0;

            /* read a connection */
            line = scanFile.nextLine();

            /* Parsing iprange with port,and trueclass for this connection */
            stSkip = new StringTokenizer(line, ",");
            ipRange = stSkip.nextToken();
            trueClass = Integer.parseInt(stSkip.nextToken());

            /* if it is parsing training connections */
            if (connectionsType.equals("do not set") == false) {
                /* assignment of the class value */
                if (trueClass == 1) {
                    userProfile = "USER01";
                    thereAreSomeUserConnection = true;
                } else {
                    userProfile = "NOT_USER01";
                    thereAreSomeNotUserConnection = true;
                }
            }
            
            /* else it is parsing connections to classify so the class value must not be set */
            else {
                userProfile = "do not set";
            }

            /* Initializing the string tokenizer to parse netflows */
            stNetFlows = new StringTokenizer(line, "]");
            String s1 = stNetFlows.nextToken("[");
            s1 = line.substring(s1.length());
            stNetFlows = new StringTokenizer(s1, "]");

            /* inner while: the rest of the method is common to both test and train connections
             * this while scans all netflows into a single connection */
            while (stNetFlows.hasMoreTokens()) {

                netFlow = stNetFlows.nextToken("[");
                netFlow = netFlow.substring(0, netFlow.length() - 1);
                stSingleNetflow = new StringTokenizer(netFlow, ",");
                /* check whether is an ingoing netflow or outgoing netflow */
                direction = (stSingleNetflow.nextToken()).substring(0, 1);
                if (direction.equals("0")) { /* is ingoing netflow */
                    inNetflowNumber++;
                    in_gap += Float.parseFloat(stSingleNetflow.nextToken());
                    support = Float.parseFloat(stSingleNetflow.nextToken());
                    avg_byte_inNetflows += support;
                    tot_byte_inNetflows += support;
                    support = Float.parseFloat(stSingleNetflow.nextToken());
                    avg_pkt_inNetflows += support;
                    tot_pkt_inNetflows += support;
                    support = Float.parseFloat(stSingleNetflow.nextToken());
                    avg_duration += support;
                    tot_duration += support;
                } else { /* else is an outgoing netflow */
                    outNetFlowNumber++;
                    out_gap += Float.parseFloat(stSingleNetflow.nextToken());
                    support = Float.parseFloat(stSingleNetflow.nextToken());
                    avg_byte_outNetflows += support;
                    tot_byte_outNetflows += support;
                    support = Float.parseFloat(stSingleNetflow.nextToken());
                    avg_pkt_outNetflows += support;
                    tot_pkt_outNetflows += support;
                    support = Float.parseFloat(stSingleNetflow.nextToken());
                    avg_duration += support;
                    tot_duration += support;
                }

            } //end of the inner while: continues parsing another netflow of this connection, or quits if there aren't

            /* calculates averages where is required, and other fields */
            if (inNetflowNumber != 0 && outNetFlowNumber != 0) {
                direction = "2";
            }
            
            
            in_gap = (inNetflowNumber > 0 ? (in_gap / inNetflowNumber): 0 );
            out_gap = (outNetFlowNumber > 0 ? (out_gap / outNetFlowNumber):0);

            avg_byte_inNetflows = (inNetflowNumber > 0 ? (avg_byte_inNetflows / inNetflowNumber): 0);
            avg_pkt_inNetflows = (inNetflowNumber > 0 ? (avg_pkt_inNetflows / inNetflowNumber):0);

            avg_byte_outNetflows = (outNetFlowNumber > 0 ? (avg_byte_outNetflows / outNetFlowNumber):0);
            avg_pkt_outNetflows = (outNetFlowNumber > 0 ? (avg_pkt_outNetflows / outNetFlowNumber):0);

            avg_duration = avg_duration / (inNetflowNumber + outNetFlowNumber);
            
            /*
            in_gap = in_gap / inNetflowNumber;
            out_gap = out_gap / outNetFlowNumber;

            avg_byte_inNetflows = avg_byte_inNetflows / inNetflowNumber;
            avg_pkt_inNetflows = avg_pkt_inNetflows / inNetflowNumber;

            avg_byte_outNetflows = avg_byte_outNetflows / outNetFlowNumber;
            avg_pkt_outNetflows = avg_pkt_outNetflows / outNetFlowNumber;

            avg_duration = avg_duration / (inNetflowNumber + outNetFlowNumber);
            */

            /* all fields are ready to be put in an object of type "Attribute" (weka)
             * and then into the dataset.The following method uses weka library for doing this work. */
            instance = updateData(ipRange, trueClass, direction, in_gap, out_gap,
                    avg_byte_inNetflows, tot_byte_inNetflows, avg_byte_outNetflows, tot_byte_outNetflows,
                    avg_pkt_inNetflows, tot_pkt_inNetflows, avg_pkt_outNetflows, tot_pkt_outNetflows,
                    avg_duration, tot_duration, userProfile, dataSet);

            instanceList.add(instance);

        } //end of the extern while, continues parsing another connection, or quits if there aren't.

        /* Determines what will be returned: if there were training connections then it returns
         * the list of parsed connections only if there were both user and not_user connections.
         * On the other hand, if there were test connections, it doesn't matter the source,
         * they will be returned anyway for classification purposes.
         */
        if ((thereAreSomeNotUserConnection == true && thereAreSomeUserConnection == true)
                || (connectionsType.equals("do not set"))) {
            return instanceList;
        }  
        else { /* there were training connections, but there weren't both user and not_user ones */
            dataSet.delete(); //the method updateData has created instances which in this case must be deleted all
            return new ArrayList<Instance>(0);
        }
    }

    /**
     * This method selects the classifier that will be used for further classifications,
     * you need to specify an input parameter of type int which identify the position of the target classifier into the List
     * of classifiers instantiated into this object; by default the first three position of the List contain: 0-SVM, 1-RandomForest, 2-NaiveBayes
     * 
     * @param choice An int that identifies the classifier into the List of classifiers of this object.
     * @return true if the choice is within the List's bounds and a classifier is selected, false otherwise.
     */
    public boolean selectClassifier(int choice) {
        if (this.evaluationList.isEmpty()) {
            System.out.println("You must evaluate classifiers before selecting a one");
            return false;
        } else if (choice > u_Classifiers.size() || choice < 0) {
            System.out.println("Your choice for classifier is not valid");
            return false;
        } else {
            currentClassifier = u_Classifiers.get(choice);
            currentFmeasure = evaluationList.get(choice).fMeasure(0);
            /* the new currentClassifier may not have been built with the actual status of the dataset,
             * so the classifier needs to be rebuild when a new classification is invoked. */
            m_UpToDate = false;
            return true;
        }
    }
    
    /**
     * This method classifies a single connection that comes instantiated as an object of type Instance (weka) 
     * then it puts the results in a report called prediction.txt located into the directory of the program,
     * the report is opened in append mode and the result of classification is appended together with it's time interval;
     * WARNING: remember to delete the predictions.txt report when you redo a classification or the new results will
     * be appended to the old ones.
     * 
     * @param inputInstance An Instance object which represents the connection that will be classified
     * @param timeBand A String that represent the time interval when the connection occured.
     * @return false if no classifier has been defined or the dataset is empty, true if the classification went correct.
     * @throws FileNotFoundException
     * @throws Exception 
     * @see Instance
     */
    public boolean classifyConnection(Instance inputInstance, String timeBand) throws FileNotFoundException, Exception {

        // check whether a classifier has been defined
        if (currentClassifier == null) {
            return false;
        }
        // Check whether a training dataset has been built.
        if (usersNetFlows_Dataset.numInstances() == 0) {
            return false;
        }

        FileOutputStream file = new FileOutputStream("predictions.txt", true);
        PrintStream output = new PrintStream(file);
        Instance filteredInstance;
        double predicted;

        /* sets up filter and creates a filtered dataset  */
        remove_filter.setInputFormat(usersNetFlows_Dataset);
        Instances filteredData = Filter.useFilter(usersNetFlows_Dataset, remove_filter);
        
        /*
        if(currentClassifier instanceof LibSVM) {
            resample_filter.setInputFormat(filteredData);
            filteredData = Filter.useFilter(filteredData, resample_filter);
        }
        */
        
        /* Check whether training dataset is up to date
         * if not, then rebuilds classfier with the updated training Dataset */
        if (!m_UpToDate) {
            currentClassifier.buildClassifier(filteredData);
            m_UpToDate = true;
        }
        
        /* uses filter over the instance to classify */
        remove_filter.input(inputInstance);
        filteredInstance = remove_filter.output();
        filteredInstance.setDataset(filteredData);

        /* prediction */
        predicted = currentClassifier.classifyInstance(filteredInstance);

        // Output class value.
        output.println(timeBand + "," + inputInstance.stringValue(0) + ","
                + usersNetFlows_Dataset.classAttribute().value((int) predicted) + "," + Utils.doubleToString(currentFmeasure, 3) );
        
        file.close();
        return true;

    }
    
    /**
     * This method evaluates all the classifiers defined into the List of classifier of the given TcpIpClassifier object
     * it uses a 10-folds cross validation with seed 1; creates a txt report with results into the directory of the program.
     * 
     * @param a TcpIpClassifier object that contains the classifiers to evaluate.
     */
    public static void evaluateClassifiers(TcpIpClassifier uc) throws Exception {
        
        File f = new File("./evaluations reports/");
        if(f.exists() == false) {
            (new File("evaluations reports")).mkdir();
        }
        FileOutputStream file = new FileOutputStream(".//evaluations reports//"+"REPORT_" + uc.usersNetFlows_Dataset.relationName() + ".txt");
        PrintStream output = new PrintStream(file);
        int numberFolds;

        Attribute att = uc.usersNetFlows_Dataset.attribute("USER PROFILE");
        uc.usersNetFlows_Dataset.setClass(att);
        Evaluation eval;
        Iterator<Classifier> it = uc.u_Classifiers.iterator();
        Attribute attribute = uc.usersNetFlows_Dataset.classAttribute();
        Instances filteredData;
        
        /* for reflection */
        Classifier classifier;
        Class clazz;
        Constructor constructor;

        /* for every classifier defined into the TcpIpClassifier object */
        while (it.hasNext()) {

            /* the weka's crossValidateModel implementation works with untrained classifiers,
             * thus when this method is launched, it must create an untrained version of the classifier
             */
            eval = new Evaluation(uc.usersNetFlows_Dataset);
            classifier = it.next();
            clazz = classifier.getClass();
            constructor = clazz.getConstructor();
            classifier = (Classifier) constructor.newInstance();
            
            /* sets up the remove filter */
            uc.remove_filter.setInputFormat(uc.usersNetFlows_Dataset);
            filteredData = Filter.useFilter(uc.usersNetFlows_Dataset, uc.remove_filter);

            /* determining the number of folds for the cross evaluation, it depends on the number of connections */
            if (filteredData.numInstances() >= 10) {
                numberFolds = 10;
            } else if (filteredData.numInstances() >= 5) {
                numberFolds = 5;
            } else {
                numberFolds = 2;
            }
            
            /* in case of a LibSvm classifier, then sets up the resample filter
            if(classifier instanceof LibSVM) {
                uc.resample_filter.setInputFormat(filteredData);
                filteredData = Filter.useFilter(filteredData, uc.resample_filter);
            }
            /* evaluation */
            eval.crossValidateModel(classifier, filteredData, numberFolds, new Random(1));
            
            /* adding the evaluation obj to the list, for further consultations */
            uc.evaluationList.add(eval);
            
            /*shows results */
            output.println("!!!!!!!!!!!!!! EVALUATION OF A " + classifier.toString().substring(0, 22) + " CLASSIFIER !!!!!!!!!!!!!!");
            output.println("Name of Dataset: " + uc.usersNetFlows_Dataset.relationName());
            
            if(classifier instanceof LibSVM) {
                LibSVM lib = (LibSVM) classifier;
                String[] opzioni = lib.getOptions();
                for(int i=0; i<opzioni.length; i++) {
                    output.print(opzioni[i]+" ");
                }
                output.println("");
            }
            
            output.println("\n<----------CORRECT - INCORRECT---------->:");
            output.println("Correctly Classified Instances: " + Utils.doubleToString(eval.correct(), 3) + "    " + Utils.doubleToString(eval.pctCorrect(), 2) + "%");
            output.println("Incorrectly Classified Instances: " + Utils.doubleToString(eval.incorrect(), 3) + "    " + Utils.doubleToString(eval.pctIncorrect(), 2) + "%");

            output.println("\n<----------F-MEASURE----------->:");
            output.println("" + attribute.value(0) + ": " + Utils.doubleToString(eval.fMeasure(0), 3));
            output.println("" + attribute.value(1) + ": " + Utils.doubleToString(eval.fMeasure(1), 3));

            output.println("\n<----------PRECISION----------->:");
            output.println("" + attribute.value(0) + ": " + Utils.doubleToString(eval.precision(0), 3));
            output.println("" + attribute.value(1) + ": " + Utils.doubleToString(eval.precision(1), 3));

            output.println("\n<----------RECALL----------->:");
            output.println("" + attribute.value(0) + ": " + Utils.doubleToString(eval.recall(0), 3));
            output.println("" + attribute.value(1) + ": " + Utils.doubleToString(eval.recall(1), 3));
            
            output.println("\n<----------ROC AREA----------->:");
            output.println(Utils.doubleToString(eval.areaUnderROC(0), 3));
           
            output.println(eval.toMatrixString("\n<----------CONFUSION MATRIX---------->:"));
            output.print("\n\n");
        }


    }
    
    
    
    /**
     * This method generates a ".arff" of the dataset of this object, you could find it into the directory of the program.
     * 
     * @throws Exception 
     */
    public void generateArff() throws Exception {
        DataSink.write("" + usersNetFlows_Dataset.relationName() + ".arff", usersNetFlows_Dataset);
    }
    
    /**
     * This method returns the dataset of this object.
     * 
     * @return An object of type Instances (weka) which is the dataset of this object.
     * @see Instances
     */
    public Instances getDataSet() {
        return this.usersNetFlows_Dataset;
    }
    
    /**
     * This method sets the dataset of this object using the one given in input;
     * WARNING: the semantics of the new dataset must be the same as that of a dataset which comes from
     * a TcpIpClassifier object.
     * 
     * @param dataSet An object of type Instances (weka) that represents a dataset of connections
     * whose semantics must follow the one of a dataset coming from a TcpIpClassifier object.
     */
    public void setDataset(Instances dataSet) {
        this.usersNetFlows_Dataset = dataSet;
        this.m_UpToDate=false;
    }
    
    /**
     * This method returns a List of evaluation objects (weka) where each one contains all the information generated
     * from a 10-folds cross validations of a classifier instantiated into this object.
     * 
     * @return a List of Evaluations objects
     * @see Evaluation
     */
    public List<Evaluation> getEvaluationList() {
        return evaluationList;
    }
    
    /**
     * This method redefines equals from class Object:
     * two TcpIpClassifier objects are equals if relation names of their datasets are equals.
     * 
     * @param uc An object to test with this object
     * @return false if the given object is not an instance of class TcpIpClassifier or the objects are not equal,
     * true if they are equal according to the relation name of their datasets.
     */
    @Override
    public boolean equals(Object uc) {
        if (uc == null) {
            return false;
        } else if (uc instanceof TcpIpClassifier) {
            boolean result = (this.usersNetFlows_Dataset.relationName()).equals(((TcpIpClassifier) uc).usersNetFlows_Dataset.relationName());
            return result;
        } else {
            return false;
        }
    }

    /* da controllare, non capisco se l'hash code definito cosi abbia senso */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.usersNetFlows_Dataset);
        return hash;
    }
}
