package test_package;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import tcpIpClassifier.MngTcpIpClassifiers;
import tcpIpClassifier.TcpIpClassifier;
import tcpIpClassifier.TimeIntervals;

/**
 * This class contains a main for using the tcpIpClassifier package
 * 
 * @author Giulio Cattivera
 */
public class Main {
    
     public static void main(String[] args) throws FileNotFoundException, InterruptedException, Exception {
        
        MngTcpIpClassifiers classifiers = new MngTcpIpClassifiers();
        TimeIntervals timeIntervals;
        TcpIpClassifier tempTcpIpClassifier; 
        
        /* creation, learning, evaluation of a set of user classifiers, one for each ipRange (application).
        /* then classifications of test connections */
        classifiers.parseAllTrainingConnections("/home/lord/Scrivania/tesi/userClassifier/Dataset train/utente 128");       
        classifiers.evaluateAll();
        classifiers.selectClassifierForAll(1); //select a type of classifier you want to use (0 LibSVM, 1 Random Forests, 2 NaiveBayes)
        classifiers.classifyConnections("/home/lord/Scrivania/tesi/userClassifier/Connections Test/utente 128");
        classifiers.generateAllArff();
        
        /* now it's time to create timeIntervals datasets */
        //finds true class values for each time band
        Map<String,String> classValues = TimeIntervals.calculatesTrueClass("/home/lord/Scrivania/tesi/userClassifier/Connections Test/utente 128");
        
        //creating an array of all ipRanges (applications) addressed by the user
        int k=0;
        Iterator it = classifiers.iterator();
        String[] ipRanges = new String[classifiers.getTcpIpClassifierSet().size()];
        while(it.hasNext()) {
            tempTcpIpClassifier= (TcpIpClassifier) it.next();
            ipRanges[k] = tempTcpIpClassifier.getDataSet().relationName();
            k++;
        }
        
        //ready to build a timeIntervals dataset for this user
        timeIntervals = new TimeIntervals(ipRanges, "user 128 timeIntervals RandomForests dataset");
        timeIntervals.parserTrainingData("/home/lord/NetBeansProjects/UserClassifier/predictions.txt", classValues);
        timeIntervals.generateTrainArff(); //now you can use this dataset with a third-party application
                                           //to train some classifier.
        
        classifiers.extractPerformanceSumUp("/home/lord/NetBeansProjects/UserClassifier/evaluations reports");
        
        /* generating test time intervals dataset
        Scanner input = new Scanner(System.in);
        System.out.println("Cambia prediction.txt e inserisci un intero");
        int n = input.nextInt();
        mtic.classifyConnections("/home/lord/Scrivania/tesi/userClassifier/Connections Test/utente 140 test");
        ti.generateTestArff("/home/lord/NetBeansProjects/UserClassifier/predictions.txt", ipRanges);
        */

    }
    
}
